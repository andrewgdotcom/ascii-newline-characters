---
title: "ASCII Newline Characters"
category: info
docname: draft-gallagher-ascii-newline-characters-00
ipr: trust200902
area: sec
workgroup: none
keyword: Internet-Draft
submissionType: IETF
venue:
  type: "Individual"
  mail: "andrewg@andrewg.com"
  repo: "https://gitlab.com/andrewgdotcom/ascii-newline-characters"
  latest: "https://andrewgdotcom.gitlab.io/draft-gallagher-ascii-newline-characters"
stand_alone: yes
pi: [toc, sortrefs, symrefs]
author:
 -
    fullname: Andrew Gallagher
    organization: PGPKeys.EU
    email: andrewg@andrewg.com
    role: editor
normative:
  ISO646:
    target: https://www.iso.org/standard/4777.html
    title: ISO/IEC 646
informative:
  ISO10646:
    target: https://www.iso.org/standard/76835.html
    title: ISO/IEC 10646
  SMTP-SMUGGLING:
    target: https://sec-consult.com/blog/detail/smtp-smuggling-spoofing-e-mails-worldwide/
    title: SMTP Smuggling

--- abstract

This document specifies a backwards-compatible common interpretation of the [ISO646] C0 newline characters LF and CR (0x0a and 0x0d) to improve compatibility between systems.

--- middle

# Introduction {#introduction}

Charset conventions have been a bottomless source of incompatibility since time immemorial.
While most computer vendors have now standardised on [ISO10646] encodings, there remains one significant source of vendor-dependent incompatibility.

Incompatible encodings of line endings date from the days of teletypes, and were held over into the computing era as competing vendors adopted them in an uncoordinated manner.
Even today, inconsistent treatment of line endings results in countless interoperability issues, many of which have security implications (e.g. [SMTP-SMUGGLING]).

Inconsistent standardisation has meant that `LF` and `CRLF` line endings are both endemic in the wild.
Other conventions have been supported historically, however these are now comparatively rare.
Since `LF` is present in both modern conventions, the obvious solution is to specify `LF` as the newline character, while allowing `CR` for backwards compatibility.
This avoids the need to re-encode every Unix-standard file in existence.

# Conventions and Definitions {#conventions-definitions}

{::boilerplate bcp14-tagged}

Unless otherwise stated, "text" refers both to plaintext data formats and any other electronic data format that follows plaintext conventions, including but not limited to: source code, shell scripts, configuration files, web pages, internet protocols, ((add more)).

# The Line Feed (LF) and Carriage Return (CR) Characters {#line-feed-carriage-return}

In a text document or data stream, the Line Feed character (`LF`, ASCII 0x0a) MUST be interpreted as a newline character (line separator), whether or not it is preceded by a Carriage Return character.

In a text document or data stream, the Carriage Return character (`CR`, ASCII 0x0d) MUST be interpreted as horizontal whitespace.
Further, a trailing `CR` at the end of a line (i.e. immediately before an `LF`) MUST be ignored.

# Backwards Compatibility {#backwards-compatibility}

A generating application MAY prefix `LF` with `CR` for backwards compatibility purposes, and SHOULD do so if specified by an earlier RFC.
A receiving application MUST NOT require `LF` to be preceded by `CR`, except where a `CRLF` canonical form is defined for the purposes of verifying digital signatures or comparing differences.

# Security Considerations {#security-considerations}

A consistent interpretation as described above would have prevented countless security issues, such as [SMTP-SMUGGLING], ((add more)).

# IANA Considerations {#iana-considerations}

None.

--- back

# Acknowledgments

The author would like to thank Dennis Ritchie, for getting it right the first time.
